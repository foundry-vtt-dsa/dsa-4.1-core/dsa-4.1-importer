import App from '../ui/ImporterDialog.svelte'
import { getGame } from './utils'

export class DSAImporterDialog extends Application {
  component: App

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      dragDrop: [
        { dragSelector: null, dropSelector: 'textarea.importer-input' },
      ],
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async _renderInner(_data: object) {
    const inner = $('<div class="svelte-app"></div>')

    this.component = new App({
      target: $(inner).get(0) as HTMLElement,
      props: {
        actors: getGame().actors?.contents as any,
        foundryApp: this as any,
      },
    })

    return inner
  }
}

Hooks.once('ready', async function () {
  getGame().importer = () => {
    new DSAImporterDialog({
      title: `DSA Character Importer`,
    }).render(true)
  }
})

Hooks.on('renderActorDirectory', async (app, html) => {
  const game = getGame()
  if (game.user?.hasPermission('ACTOR_CREATE')) {
    const importerButton = $(
      `<button style="margin: 4px; padding: 1px 6px;"><i class="fas fa-file-import"> ${game.i18n.localize(
        'DSA.import'
      )}</i></button>`
    )
    html.find('.directory-footer').append(importerButton)

    importerButton.on('click', () => game.importer())
  }
})

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ClientSettings {
    interface Values {
      'dsa-41-importer.talent-pack': string
      'dsa-41-importer.combattalent-pack': string
      'dsa-41-importer.specialability-pack': string
      'dsa-41-importer.spell-pack': string
      'dsa-41-importer.meleeweapon-pack': string
      'dsa-41-importer.rangedweapon-pack': string
      'dsa-41-importer.shield-pack': string
      'dsa-41-importer.armor-pack': string
    }
  }
}

Hooks.once('ready', async function () {
  const game = getGame()
  const packChoices = {}

  game.packs.forEach((p) => {
    packChoices[p.collection] = `${p.metadata.label} (${p.metadata.package})`
  })

  game.settings.register('dsa-41-importer', 'talent-pack', {
    name: `dsa-41-importer.settings.talentPack`,
    hint: `dsa-41-importer.settings.talentPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.talent',
  })

  game.settings.register('dsa-41-importer', 'combattalent-pack', {
    name: `dsa-41-importer.settings.combattalentPack`,
    hint: `dsa-41-importer.settings.combattalentPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.combattalent',
  })

  game.settings.register('dsa-41-importer', 'specialability-pack', {
    name: `dsa-41-importer.settings.specialabilityPack`,
    hint: `dsa-41-importer.settings.specialabilityPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.specialability',
  })

  game.settings.register('dsa-41-importer', 'spell-pack', {
    name: game.i18n.localize(`DSA.settings.spellPack`),
    hint: game.i18n.localize(`DSA.settings.spellPackHint`),
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-41.spell',
  })

  game.settings.register('dsa-41-importer', 'liturgy-pack', {
    name: `dsa-41-importer.settings.liturgyPack`,
    hint: `dsa-41-importer.settings.liturgyPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.liturgy',
  })

  game.settings.register('dsa-41-importer', 'meleeweapon-pack', {
    name: `dsa-41-importer.settings.meleeweaponPack`,
    hint: `dsa-41-importer.settings.meleeweaponPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.meleeweapon',
  })

  game.settings.register('dsa-41-importer', 'rangedweapon-pack', {
    name: `dsa-41-importer.settings.rangedweaponPack`,
    hint: `dsa-41-importer.settings.rangedweaponPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.rangedweapon',
  })

  game.settings.register('dsa-41-importer', 'armor-pack', {
    name: `dsa-41-importer.settings.armorPack`,
    hint: `dsa-41-importer.settings.armorPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.armor',
  })

  game.settings.register('dsa-41-importer', 'shield-pack', {
    name: `dsa-41-importer.settings.shieldPack`,
    hint: `dsa-41-importer.settings.shieldPackHint`,
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-4.1.shield',
  })
})
