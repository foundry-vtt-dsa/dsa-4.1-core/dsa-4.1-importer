export class ImportOptions {
    importEquipment: boolean;
    showCharacterAfterImport: boolean;
}
