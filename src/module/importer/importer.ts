import type {
  CharacterData,
  NormalTalent,
  CombatTalent,
  Advantage,
  Disadvantage,
  LinguisticTalent,
  Spell,
  SpecialAbility,
  GenericItem,
  Liturgy,
  MeleeWeapon,
  Armor,
  Shield,
  RangedWeapon,
  Talent,
  // Test3d20,
} from '../model/types'

import { ImportOptions } from './importOptions'
import { getPacks } from './packs'
import { getGame } from '../utils'
import { getHeldensoftwareCharacterData } from '../heldensoftware/heldensoftware-import'

async function getActorData(actorId: string, characterName: string) {
  const game = getGame()
  if (actorId) {
    const actor = game.actors?.get(actorId)
    if (actor?.name != characterName) {
      console.log('Name does not fit!')
      return
    }
    await createActorBackup(actor)
    return actor
  } else {
    return Actor.create({
      name: characterName,
      type: 'character',
      system: {},
    })
  }
}

async function createActorBackup(actor) {
  const backupData = duplicate(actor)
  backupData.name = backupData.name + ' Backup'
  await Actor.create(backupData)
}

async function getTalentList(): Promise<Talent[]> {
  const game = getGame()
  const TalentCompendium = 'dsa-41.talent'
  const talentPack = game.packs.get(TalentCompendium)
  const talentPackEntries =
    ((await talentPack?.getDocuments()) as StoredDocument<any>) || []
  return talentPackEntries.map((talent) => ({
    type: talent.system.type,
    name: talent.name,
    category: talent.system.category,
    test: {
      firstAttribute: talent.system.test.firstAttribute,
      secondAttribute: talent.system.test.secondAttribute,
      thirdAttribute: talent.system.test.thirdAttribute,
    },
    sid: talent.system.sid,
    linguisticType: ['scripture', 'language'].includes(talent.system.type)
      ? talent.system.type
      : undefined,
    complexity: talent.system.complexity,
    value: 0,
    effectiveEncumbarance: talent.system.effectiveEncumbarance,
  }))
}

async function getSpellList(): Promise<Spell[]> {
  const game = getGame()
  const SpellCompendium = game.settings.get('dsa-41-importer', 'spell-pack')
  const spellPack = game.packs.get(SpellCompendium)
  const spellPackEntries =
    (await spellPack?.getDocuments()) as StoredDocument<any>
  if (spellPackEntries) {
    return spellPackEntries.map((spell) => ({
      name: spell.name,
      test: {
        firstAttribute: spell.system.test.firstAttribute,
        secondAttribute: spell.system.test.secondAttribute,
        thirdAttribute: spell.system.test.thirdAttribute,
      },
      sid: spell.system.sid,
      astralCost: spell.system.astralCost,
      range: spell.system.range,
      lcdPage: spell.system.lcdPage,
      value: 0,
      type: 'spell',
    }))
  }
  return []
}

async function getCombatTalentList(): Promise<CombatTalent[]> {
  const game = getGame()
  const TalentCompendium = 'dsa-41.combattalent'
  const talentPack = game.packs.get(TalentCompendium)
  const talentPackEntries =
    ((await talentPack?.getDocuments()) as StoredDocument<any>) || []
  return talentPackEntries.map((talent) => ({
    attack: 0,
    category: talent.system.category,
    value: 0,
    type: talent.system.type,
    name: talent.name,
    combatType: talent.system.combat.category,
    sid: talent.system.sid,
    effectiveEncumbarance: talent.system.effectiveEncumbarance,
  }))
}

async function getSpecialAbilityList(): Promise<SpecialAbility[]> {
  const game = getGame()
  const SpecialAbilityCompendium = 'dsa-41.specialability'
  const specialAbilityPack = await game.packs.get(SpecialAbilityCompendium)
  const specialAbilityPackEntries =
    (await specialAbilityPack?.getDocuments()) as StoredDocument<any>
  if (specialAbilityPackEntries) {
    return specialAbilityPackEntries.map(
      (specialAbility) =>
        ({
          name: specialAbility.name,
          sid: specialAbility.system.sid,
        } as SpecialAbility)
    )
  }
  return []
}

async function getLiturgyList(): Promise<Liturgy[]> {
  const game = getGame()
  const LiturgyCompendium = 'dsa-41.liturgy'
  const liturgyPack = await game.packs.get(LiturgyCompendium)
  const liturgyPackEntries =
    (await liturgyPack?.getDocuments()) as StoredDocument<any>
  if (liturgyPackEntries) {
    return liturgyPackEntries.map(
      (liturgy) =>
        ({
          name: liturgy.name,
          sid: liturgy.system.sid,
          description: liturgy.system.description,
          castTime: {
            duration: liturgy.system.castTime.duration,
            unit: liturgy.system.castTime.unit,
            info: liturgy.system.castTime.info,
          },
          effectTime: {
            duration: liturgy.system.effectTime.duration,
            unit: liturgy.system.effectTime.unit,
            info: liturgy.system.effectTime.info,
          },
          targetClasses: liturgy.system.targetClasses,
          range: liturgy.system.range,
          variants: liturgy.system.variants,
          degree: liturgy.system.degree,
          castType: liturgy.system.castType,
          llPage: liturgy.system.llPage,
        } as Liturgy)
    )
  }
  return []
}

export async function importCharacter(
  xmlString,
  actorId,
  options: ImportOptions
) {
  const packs = await getPacks()
  const data = getHeldensoftwareCharacterData(
    xmlString,
    packs,
    options.importEquipment
  )
  const actor = await getActorData(actorId, data.name)
  await renameSkillToSpecialAbility(actor)
  await deleteItemTypes(actor, [
    'spell',
    'specialAbility',
    'talent',
    'language',
    'scripture',
    'combatTalent',
  ])
  await deleteLiturgies(actor)

  await importActorData(actor, data)
  await importAdvantages(actor, data.advantages)
  await importDisdvantages(actor, data.disadvantages)
  await importSpecialAbilities(actor, data.specialAbilities)
  await importTalents(actor, data.talents)
  await importCombatTalents(actor, data.combatTalents)
  await importLinguisticTalents(actor, data.linguisticTalents)
  await importSpells(actor, data.spells)
  await importLiturgies(actor, data.liturgies)

  if (options.importEquipment) {
    await deleteMeleeWeapons(actor)
    await deleteRangedWeapons(actor)
    await deleteArmors(actor)
    await deleteShields(actor)
    await deleteGenericItems(actor)
    await importMeleeWeapons(actor, data.meleeWeapons)
    await importRangedWeapons(actor, data.rangedWeapons)
    await importArmors(actor, data.armors)
    await importShields(actor, data.shields)
    await importGenericItems(actor, data.genericItems)
  }

  if (options.showCharacterAfterImport) {
    await actor?.sheet?.render(true);
  }
}

async function importActorData(actor, data: CharacterData) {
  const actorData = {
    system: {
      base: {},
    },
  }

  setAttributes(actorData, data)
  setCombatAttributes(actorData, data)
  setResources(actorData, data)

  actor.update(actorData)
}

async function renameSkillToSpecialAbility(actor) {
  const itemUpdateData = actor.items
    .filter((item) => item.type === 'skill')
    .map((item) => ({ _id: item.id, type: 'specialAbility' }))
  actor.updateEmbeddedDocuments('Item', itemUpdateData)
}

async function deleteLiturgies(actor) {
  deleteItemTypes(actor, ['liturgy'])
}

async function deleteGenericItems(actor) {
  deleteItemTypes(actor, ['genericItem'])
}

async function deleteMeleeWeapons(actor) {
  deleteItemTypes(actor, ['meleeWeapon'])
}

async function deleteRangedWeapons(actor) {
  deleteItemTypes(actor, ['rangedWeapon'])
}

async function deleteShields(actor) {
  deleteItemTypes(actor, ['shield'])
}

async function deleteArmors(actor) {
  deleteItemTypes(actor, ['armor'])
}

async function deleteItemTypes(actor, types) {
  const deleteItemIds = actor.items
    .filter((item) => types.includes(item.type))
    .map((item) => item.id)
  return actor.deleteEmbeddedDocuments('Item', deleteItemIds)
}

function setResources(actorData, data: CharacterData) {
  const resourceNames = [
    'vitality',
    'endurance',
    'astralEnergy',
    'karmicEnergy',
  ]
  actorData.system.base.resources = {}
  resourceNames.forEach((resource) => {
    actorData.system.base.resources[resource] = {
      max: data.resources[resource].max,
    }
  })
  // actorsystem.base.resources = {
  //   vitality: data.resources.vitality,
  //   endurance: data.resources.endurance,
  //   astralEnergy: data.resources.astralEnergy,
  //   karmicEnergy: data.resources.karmicEnergy,
  // }
}

function setCombatAttributes(actorData, data: CharacterData) {
  actorData.system.base.combatAttributes = {
    active: {
      baseInitiative: {
        value: data.combatAttributes.baseInitiative,
      },
      baseAttack: {
        value: data.combatAttributes.baseAttack,
      },
      baseParry: {
        value: data.combatAttributes.baseParry,
      },
      baseRangedAttack: {
        value: data.combatAttributes.baseRangedAttack,
      },
    },
    passive: {
      magicResistance: {
        value: data.combatAttributes.magicResistance,
      },
    },
  }
}

function setAttributes(actorData, data: CharacterData) {
  actorData.system.base.basicAttributes = {
    courage: {
      value: data.attributes.courage,
    },
    cleverness: {
      value: data.attributes.cleverness,
    },
    intuition: {
      value: data.attributes.intuition,
    },
    charisma: {
      value: data.attributes.charisma,
    },
    dexterity: {
      value: data.attributes.dexterity,
    },
    agility: {
      value: data.attributes.agility,
    },
    constitution: {
      value: data.attributes.constitution,
    },
    strength: {
      value: data.attributes.strength,
    },
  }
}

async function updateOrCreateItems(actor, itemsData) {
  const itemsUpdateCreateData = itemsData.map((itemData) => {
    const ownedItem = actor.items.find((item) => item.name == itemData.name)
    return {
      id: ownedItem?.id,
      ...itemData,
    }
  })
  const updateData = itemsUpdateCreateData
    .filter((itemUpdate) => itemUpdate.id)
    .map((itemUpdate) => ({ ...itemUpdate, _id: itemUpdate.id }))
  const createData = itemsUpdateCreateData
    .filter((itemUpdate) => itemUpdate.id === undefined)
    .map((itemUpdate) => itemUpdate)
  await actor.updateEmbeddedDocuments('Item', updateData)
  await actor.createEmbeddedDocuments('Item', createData)
}

class ImportError extends Error {
  entryName: string
  entryType: string

  constructor(entryName, entryType, ...params) {
    super(...params)
    this.entryName = entryName
    this.entryType = entryType
  }
}

interface ItemDataConverter<Item> {
  convert(item: Item): any
}

function convertItem<Item>(item: Item, converter: ItemDataConverter<Item>) {
  let itemData
  try {
    itemData = converter.convert(item)
  } catch (err) {
    if (err instanceof ImportError) {
      console.log(
        'Import Error: Cannot add ' + err.entryType + ': ' + err.entryName
      )
    } else {
      throw err
    }
  }
  return itemData
}

async function importItems<Item>(
  actor,
  items: Item[],
  converter: ItemDataConverter<Item>
) {
  const itemsData = items.map((item) => convertItem(item, converter))

  await updateOrCreateItems(actor, itemsData)
}

class AdvantageDataConverter implements ItemDataConverter<Advantage> {
  convert(item: Advantage) {
    return {
      type: 'advantage',
      name: item.name,
      system: {
        value: item.value,
      },
    }
  }
}

async function importAdvantages(actor, advantages: Advantage[]) {
  await importItems<Advantage>(actor, advantages, new AdvantageDataConverter())
}

class DisadvantageDataConverter implements ItemDataConverter<Disadvantage> {
  convert(item: Disadvantage) {
    return {
      type: 'disadvantage',
      name: item.name,
      system: {
        negativeAttribute: item.isNegativeAttribute,
        value: item.value,
      },
    }
  }
}

async function importDisdvantages(actor, disadvantages: Disadvantage[]) {
  await importItems<Disadvantage>(
    actor,
    disadvantages,
    new DisadvantageDataConverter()
  )
}

class SpecialAbilityDataConverter implements ItemDataConverter<SpecialAbility> {
  convert(item: SpecialAbility) {
    return {
      type: 'specialAbility',
      name: item.name,
      system: {
        sid: item.sid,
      },
    }
  }
}

async function importSpecialAbilities(
  actor,
  specialAbilities: SpecialAbility[]
) {
  await importItems<SpecialAbility>(
    actor,
    specialAbilities,
    new SpecialAbilityDataConverter()
  )
}

class TalentDataConverter implements ItemDataConverter<NormalTalent> {
  convert(item: NormalTalent) {
    return {
      type: 'talent',
      name: item.name,
      system: {
        value: item.value,
        category: item.category,
        type: item.type,
        sid: item.sid,
        test: {
          firstAttribute: item.test?.firstAttribute,
          secondAttribute: item.test?.secondAttribute,
          thirdAttribute: item.test?.thirdAttribute,
        },
        effectiveEncumbarance: item.effectiveEncumbarance,
      },
    }
  }
}

async function importTalents(actor, talents: NormalTalent[]) {
  await importItems<NormalTalent>(actor, talents, new TalentDataConverter())
}

class CombatTalentDataConverter implements ItemDataConverter<CombatTalent> {
  getCombatData(item: CombatTalent) {
    switch (item.combatType) {
      case 'melee':
        return {
          category: 'melee',
          attack: item.attack,
          parry: item.parry,
        }
      case 'unarmed':
        return {
          category: 'unarmed',
          attack: item.attack,
          parry: item.parry,
        }
      case 'ranged':
        return {
          category: 'ranged',
          rangedAttack: item.rangedAttack,
        }
      case 'special':
        return {
          category: 'special',
          attack: item.attack,
        }
    }
  }

  convert(item: CombatTalent) {
    return {
      type: 'combatTalent',
      name: item.name,
      system: {
        value: item.value,
        category: item.category,
        type: item.type,
        sid: item.sid,
        effectiveEncumbarance: item.effectiveEncumbarance,
        combat: this.getCombatData(item),
      },
    }
  }
}

async function importCombatTalents(actor, combatTalents: CombatTalent[]) {
  await importItems<CombatTalent>(
    actor,
    combatTalents,
    new CombatTalentDataConverter()
  )
}

class LinguisticTalentDataConverter
  implements ItemDataConverter<LinguisticTalent> {
  convert(item: LinguisticTalent) {
    return {
      type: item.linguisticType,
      name: item.name,
      system: {
        value: item.value,
        category: item.category,
        type: item.type,
        sid: item.sid,
        complexity: item.complexity,
      },
    }
  }
}

async function importLinguisticTalents(
  actor,
  linguisticTalents: LinguisticTalent[]
) {
  await importItems<LinguisticTalent>(
    actor,
    linguisticTalents,
    new LinguisticTalentDataConverter()
  )
}

class SpellDataConverter implements ItemDataConverter<Spell> {
  convert(item: Spell) {
    return {
      type: 'spell',
      name: item.name,
      system: {
        value: item.value,
        test: {
          firstAttribute: item.test?.firstAttribute,
          secondAttribute: item.test?.secondAttribute,
          thirdAttribute: item.test?.thirdAttribute,
        },
        sid: item.sid,
        astralCost: item.astralCost,
        range: item.range,
        lcdPage: item.lcdPage,
      },
    }
  }
}

async function importSpells(actor, spells: Spell[]) {
  await importItems<Spell>(actor, spells, new SpellDataConverter())
}

class GenericItemDataConverter implements ItemDataConverter<GenericItem> {
  convert(item: GenericItem) {
    return {
      type: 'genericItem',
      name: item.name,
      system: {
        value: item.value,
        price: item.price,
        weight: item.weight,
      },
    }
  }
}

async function importGenericItems(actor, genericItems: GenericItem[]) {
  await importItems<GenericItem>(
    actor,
    genericItems,
    new GenericItemDataConverter()
  )
}

class MeleeWeaponDataConverter implements ItemDataConverter<MeleeWeapon> {
  convert(item: MeleeWeapon) {
    return {
      type: 'meleeWeapon',
      name: item.name,
      data: {
        damage: item.damage,
        talent: item.talent,
        strengthMod: {
          threshold: item.strengthMod.threshold,
          hitPointStep: item.strengthMod.hitPointStep,
        },
        initiativeMod: item.initiativeMod,
        weaponMod: {
          attack: item.weaponMod.attack,
          parry: item.weaponMod.parry,
        },
        distanceClass: item.distanceClass,
        breakingFactor: item.breakingFactor,
        length: item.length,
        twoHanded: item.twoHanded,
        priviledged: item.priviledged,
        improvised: item.improvised,
        enduranceDamage: item.enduranceDamage,
        description: item.description,
        price: item.price,
        weight: item.weight,
        aaPage: item.aaPage,
      },
    }
  }
}

async function importMeleeWeapons(actor, meleeWeapons: MeleeWeapon[]) {
  await importItems<MeleeWeapon>(
    actor,
    meleeWeapons,
    new MeleeWeaponDataConverter()
  )
}

class RangedWeaponDataConverter implements ItemDataConverter<RangedWeapon> {
  convert(item: RangedWeapon) {
    return {
      type: 'rangedWeapon',
      name: item.name,
      data: {
        damage: item.damage,
        talent: item.talent,
        ranges: {
          veryClose: item.ranges.veryClose,
          close: item.ranges.close,
          medium: item.ranges.medium,
          far: item.ranges.far,
          veryFar: item.ranges.veryFar,
        },
        bonusDamages: {
          veryClose: item.bonusDamages.veryClose,
          close: item.bonusDamages.close,
          medium: item.bonusDamages.medium,
          far: item.bonusDamages.far,
          veryFar: item.bonusDamages.veryFar,
        },
        loadtime: item.loadtime,
        projectileType: item.projectileType,
        loweredWoundThreshold: item.loweredWoundThreshold,
        entangles: item.entangles,
        improvised: item.improvised,
        enduranceDamage: item.enduranceDamage,
        description: item.description,
        price: item.price,
        weight: item.weight,
        aaPage: item.aaPage,
      },
    }
  }
}

async function importRangedWeapons(actor, rangedWeapons: RangedWeapon[]) {
  await importItems<RangedWeapon>(
    actor,
    rangedWeapons,
    new RangedWeaponDataConverter()
  )
}

class ShieldDataConverter implements ItemDataConverter<Shield> {
  convert(item: Shield) {
    return {
      type: 'shield',
      name: item.name,
      data: {
        initiativeMod: item.initiativeMod,
        weaponMod: {
          attack: item.weaponMod.attack,
          parry: item.weaponMod.parry,
        },
        breakingFactor: item.breakingFactor,
        description: item.description,
        price: item.price,
        weight: item.weight,
        aaPage: item.aaPage,
      },
    }
  }
}

async function importShields(actor, rangedWeapons: Shield[]) {
  await importItems<Shield>(actor, rangedWeapons, new ShieldDataConverter())
}

class ArmorDataConverter implements ItemDataConverter<Armor> {
  convert(item: Armor) {
    return {
      type: 'armor',
      name: item.name,
      data: {
        armorClass: item.armorClass,
        encumbarance: item.encumbarance,
        zonedArmorClass: {
          head: item.zonedArmorClass.head,
          chest: item.zonedArmorClass.chest,
          torso: item.zonedArmorClass.torso,
          back: item.zonedArmorClass.back,
          leftArm: item.zonedArmorClass.leftArm,
          rightArm: item.zonedArmorClass.rightArm,
          leftLeg: item.zonedArmorClass.leftLeg,
          rightLeg: item.zonedArmorClass.rightLeg,
          tail: item.zonedArmorClass.tail,
          total: item.zonedArmorClass.total,
        },
        zonedEncumbrance: item.zonedEncumbrance,
        description: item.description,
        price: item.price,
        weight: item.weight,
        aaPage: item.aaPage,
      },
    }
  }
}

async function importArmors(actor, armors: Armor[]) {
  await importItems<Armor>(actor, armors, new ArmorDataConverter())
}

class LiturgyDataConverter implements ItemDataConverter<Liturgy> {
  convert(item: Liturgy) {
    return {
      type: 'liturgy',
      name: item.name,
      system: {
        description: item.description,
        castTime: {
          duration: item.castTime.duration,
          unit: item.castTime.unit,
          info: item.castTime.info,
        },
        effectTime: {
          duration: item.castTime.duration,
          unit: item.castTime.unit,
          info: item.castTime.info,
        },
        targetClasses: item.targetClasses,
        range: item.range,
        variants: item.variants,
        degree: item.degree,
        castType: item.castType,
        llPage: item.llPage,
      },
    }
  }
}

async function importLiturgies(actor, liturgies: Liturgy[]) {
  await importItems<Liturgy>(actor, liturgies, new LiturgyDataConverter())
}
